Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users, :path_names => { :sign_in => 'login', :sign_out => 'logout', :sign_up => 'register' } #:path_names => { :new => 'make', :edit => 'change' }
  resources :portfolios
  resources :comments

  root to: 'pages#home'
  get 'pages/home'
  get 'about-me', to: 'pages#about'  
  get 'contact', to: 'pages#contact'
  # get 'technlogy', to: 'portfolios#technlogy'


  resources :blogs do
  	member do
  		get :toggle_status
  		
  	end
  end
 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
