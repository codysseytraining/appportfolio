# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#we are creatig the sample datas here for test here 
#we can put datas while applications need to start at begining are here


1.times do |admin_details|
	User.create!(
		email: "ashokpoudel2051@gmail.com",
		name: "admin",
		password: "123456",
		role: "admin"
		)
end

 Portfolio.create(
           title: "latest Portfolio",
           subtitle: "this is subtitle",
           body: "this is the body of the portfolio"
 	)