class AddAttachmentMainImageToPortfolios < ActiveRecord::Migration[5.2]
  def self.up
    change_table :portfolios do |t|
      t.attachment :main_image
    end
  end

  def self.down
    remove_attachment :portfolios, :main_image
  end
end
