class Blog < ApplicationRecord
	enum status:{draft: 0, published: 1} #added for enum action
	extend FriendlyId
	friendly_id :title, use: :slugged  #title ko slug banauna ko lagi name lai title le replace garya
	validates_presence_of :title, :body #to validate the data

	belongs_to :topic, optional: true
	belongs_to :user
	has_many :comments, :dependent => :destroy

	after_create -> (model) { model.update(status: 0) }  #used for default status draft after creation

end
