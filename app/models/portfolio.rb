# frozen_string_literal: true

# models are defined here
class Portfolio < ApplicationRecord
  has_attached_file :main_image, styles: { large: "800x800", medium: "800x800>", thumb: "100x100#" }, default_url: "DefaultImage.png"
  # default_url: "/images/:style/missing.png"
  validates_attachment_content_type :main_image, content_type: /\Aimage\/.*\z/

  has_many :technlogies,
  dependent: :destroy  #this is for destroy all the dependencies for all portfolio items

  accepts_nested_attributes_for :technlogies,  #this two lines are for validation of nested items of technlogies on form
    reject_if: proc {|x|  x['name'].blank?}

  validates_presence_of :title, :body 

  after_initialize :set_defaults  #method to initialize the portfolio items with default placeholder
  scope :python, -> {where(title: 'python')}	

	# def self.python 
	# 	where(title: 'python')}
	# end
  def set_defaults
    self.main_image ||= 'https://via.placeholder.com/550x250'
  # self.thumb_image ||= 'https://via.placeholder.com/150x50'
  end
end
