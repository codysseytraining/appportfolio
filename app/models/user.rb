class User < ApplicationRecord
  
  has_many :blogs, :dependent => :destroy
  has_many :comments, :dependent => :destroy

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable


  validates_presence_of :name
  def first_name
  	self.name.split.first  unless self.name.nil? #self will extract the current object unless self.name is nill
  end

  def last_name
  	self.name.split.last unless self.name.nil?  #unless self.name.nil is used for remove nilclass error
  end

end
