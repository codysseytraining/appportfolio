# frozen_string_literal: true

# actions related to portfolios are defined here
class PortfoliosController < ApplicationController
  respond_to :html, :js
  # layout 'portfolio'

  def index 

    if can? :create, Portfolio
      @portfolio_items_new = Portfolio.new
      3.times { @portfolio_items_new.technlogies.build }
    end
      if params[:name]
       @portfolio_items = []
       @tec=Technlogy.all.where(name: params[:name])
       @tec.each do |x|
        @portfolio_items << x.portfolio
      end
    else
      @portfolio_items = Portfolio.all
    end

  @name_of_tech = (Technlogy.all.map { |x| x.name}).uniq 
    #finds all the technlogy used
  end

  def new
    if can? :create, Portfolio
      @portfolio_items = Portfolio.new
      3.times { @portfolio_items.technlogies.build }
    else
      redirect_to root_path
    end
  end

  def create
    if can? :create, Portfolio
      @portfolio_items = Portfolio.new(portfolio_items)
      if @portfolio_items.save
        @latest_item = Portfolio.last
        flash[:notice] = 'New portfolio item is successfully created'
        redirect_to portfolios_path
      else
        x.html { render :new }
      end
    end
  end

  def edit
    if can? :edit, Portfolio
      @portfolio_items = Portfolio.find(params[:id])
    else
      redirect_to portfolios_path
    end
  end

  def update
    if can? :update, Portfolio
      @portfolio_items = Portfolio.find(params[:id])
      if @portfolio_items.update_attributes(portfolio_items)
        redirect_to portfolios_path
      else
        render 'edit'
      end
    end
  end

  def show
    @portfolio_items = Portfolio.find(params[:id])
  end

  def destroy
    if can? :destroy, Portfolio
      @portfolio_items = Portfolio.find(params[:id])
      @portfolio_items.destroy
      redirect_to portfolios_path
    end
  end


  private

  def portfolio_items
    params.require(:portfolio).permit(
     :title, :subtitle, :body, :main_image,
     technlogies_attributes: [:id, :name]
     )
  end
end
