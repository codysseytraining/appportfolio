# frozen_string_literal: true

# actions related to comments defined here
class CommentsController < ApplicationController
  def create
    @comment = Comment.new(comment_params)
    if user_signed_in?
      @comment.user_id = current_user.id
    end
    @comment.blog_id = params[:comment][:blog_id]
    respond_to do |format|
      if @comment.save
        format.js
        format.html { redirect_to blogs_path }
        flash[:notice] = 'comment successfully added'
      else
        flash[:notice] = 'comment cant be saved'
        format.js
        format.html { redirect_to blogs_path }
      end
    end
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def index
    @comments = Comment.all
  end

  def edit
    if Comment.find(params[:id]).user_id == current_user.id
      respond_to do |format|
        @comment = Comment.find(params[:id])
        format.js
      end
    else
      redirect_to blogs_path
    end
  end

  def update
    @comment = Comment.find(params[:id])
    respond_to do |format|
      if @comment.update(comment_params)
        flash[:notice] = 'comment successfully edited'
        format.js
        format.html { redirect_to blogs_path }
      else
        render 'edit'
        format.js
      end
    end
  end

  def destroy
    respond_to do |format|
      if Comment.find(params[:id]).user_id == current_user.id
        @comment = Comment.find(params[:id])
        @comment.destroy
        flash[:notice] = 'successfully deleted message'
        format.html { redirect_to blogs_path }
        format.js
        format.js { render layout: false }
      end
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:text, :blog_id)
  end
end
