class BlogsController < ApplicationController

  before_action only: [:show, :update, :destroy]
  before_action :set_blog
  # layout "blog"

  # GET /blogs
  # GET /blogs.json
  def index
    @blogs = Blog.all
  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show
    @blog = Blog.friendly.find(params[:id])
    #edited for friendly id to make slug to the url
  end

  # GET /blogs/new
  def new
    @blog = Blog.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /blogs/1/edit
  def edit
      @blog = Blog.find_by(slug: params[:id])
       authorize! :edit, @blog
  end

  # POST /blogs
  # POST /blogs.json
  def create


 
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else

    if can? :create, Blog

      @blog = Blog.new(blog_params)
      @blog.user_id = current_user.id   #user id defined for blog
      @created_blog = @blog
      respond_to do |format|
        if @blog.save
          format.html { redirect_to @blog, notice: 'Blog was successfully created.' }
          format.js
          
        else
          format.html { render :new }
          format.js
          #format.json { render json: @blog.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to root_path
    end
  end

  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to @blog, notice: 'Blog was successfully updated.' }
        #format.json { render :show, status: :ok, location: @blog }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    
      @blog.destroy
      respond_to do |format|
        format.html { redirect_to blogs_url, notice: 'Blog was successfully destroyed.' }
      end
    
  end
      
  

def toggle_status   #this is for update publish or draft of the items
  respond_to do |format|
      @blog = Blog.find_by(slug: params[:id])
      if @blog.status == "draft"
        @blog.update(status: :published)
        format.js
      else
        @blog.update(status: :draft)
        format.js
      end
      format.html{redirect_to blogs_url, notice: 'post has successufully updated'}
  end
  #byebug
end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog
      #@blog = Blog.find(params[:id])
      @blog = Blog.find_by(slug: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_params
      
      params.require(:blog).permit(:title, :body)
    end
end