class ApplicationController < ActionController::Base

    before_action :configure_permitted_parameters, if: :devise_controller?


     before_action :set_source 


     rescue_from CanCan::AccessDenied do |exception|
      flash[:alert] = exception.message
      redirect_to root_path
    end
     def set_source
     	flash[:source] = params[:q] if params[:q] #if params present then put it on flash
     end

  #   def current_user        #for guest and current user find and show hi,guest
 	# guest = OpenStruct.new(name: "Guest User",
 	#                         first_name: "Guest", 
 	#                         last_name: "User"
 	#                         ) 
	 #    super || guest
  #   end



	protected

	  def configure_permitted_parameters #instanciation
	    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
	    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
	  end
    
end


