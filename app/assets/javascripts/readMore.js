
function readMore(event,id){
	event.preventDefault();
	
	var x = document.getElementById(`blog-body-${id}`);
	var y = document.getElementById(`truncated-blog-body-${id}`);
	
	if (y.style.display === "block"){
		
		y.style.display = "none"; 		
	}

	if (y.style.display==="none"){
		x.style.display = "block";
	}
}