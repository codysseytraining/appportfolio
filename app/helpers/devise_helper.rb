module DeviseHelper
  def devise_error_messages!
    return "" unless devise_error_messages?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t("errors.messages.not_saved",
                      :count => resource.errors.count,
                      :resource => resource.class.model_name.human.downcase)

    html = <<-HTML
    <div id="error_explanation" class="col-md-12 style-error-devise" >


      
      <ul style="list-style-type:none">
        <ul><h5>3 errors prohibited this user from being saved:</h5></ul>
        <li>Email can't be blank</li>
        <li>Password can't be blank</li>
        <li>Name can't be blank</li>
      </ul>
    </div>
   
    HTML

    html.html_safe
  end

  def devise_error_messages?
    !resource.errors.empty?
  end

end




