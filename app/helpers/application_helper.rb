module ApplicationHelper
	def login_helper

     if current_user.is_a?(User) 
       link_to "Logout", destroy_user_session_path, method: :delete
     else
        (link_to "Register account", new_user_registration_path) +
        "<br>".html_safe +
        (link_to "Login", new_user_session_path)
        
     end  

	end

  def gravatar_url(email, size)
    gravatar = Digest::MD5::hexdigest(email).downcase
    url = "http://gravatar.com/avatar/#{gravatar}.png?s=#{size}"
  end
    
end
